#!/usr/bin/env python3

import os
import aws_cdk.aws_ec2 as ec2
import aws_cdk.aws_rds as rds
import aws_cdk.core as cores
import requests
from aws_cdk import core


class CustomerReportingStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id=id, **kwargs)
 
        
        ip = requests.get("https://api.ipify.org/?format=json").json().get('ip')

        vpc = ec2.Vpc(self, "VPC",
            enable_dns_support=True,
            enable_dns_hostnames=True, 
            max_azs=2, 
            nat_gateways=1)
        
        sg = ec2.SecurityGroup(self,"SecurityGroup",vpc=vpc, allow_all_outbound=True)
        sg.add_ingress_rule(peer=ec2.Peer.ipv4(ip + '/32'), connection=ec2.Port.tcp(5432))
        sg.add_ingress_rule(sg, ec2.Port.all_traffic())

        #
        # bastion = ec2.Instance(self,"Bastion",
        #     vpc=vpc,
        #     instance_type=ec2.InstanceType.of(ec2.InstanceClass.BURSTABLE2, ec2.InstanceSize.MICRO),
        #     machine_image=ec2.MachineImage.latest_amazon_linux(),
        #     key_name='geoff',
        #     allow_all_outbound=True,
        #     security_group=sg,
        #     vpc_subnets=ec2.SubnetType.PUBLIC)
            
        # #ec2.BastionHostLinux(self,"Bastion",vpc=vpc, security_group=sg, subnet_selection=ec2.SubnetType.PUBLIC)

        # Example automatically generated without compilation. See https://github.com/aws/jsii/issues/826
        instance = rds.DatabaseInstance(self, "Instance",
            engine=rds.DatabaseInstanceEngine.postgres(version=rds.PostgresEngineVersion.VER_12_3),
            # optional, defaults to m5.large
            instance_type=ec2.InstanceType.of(ec2.InstanceClass.BURSTABLE2, ec2.InstanceSize.SMALL),
            vpc=vpc,
            max_allocated_storage=200,
            security_groups=[sg],
            publicly_accessible=True,
            vpc_subnets=ec2.SubnetType.PUBLIC
        )


app = core.App()
CustomerReportingStack(app,"CustomerReporting")
app.synth()

